MATCH (n)
RETURN n

MATCH (n:u7pasterczyk)
RETURN n

MATCH (n:u7pasterczyk)
DELETE n

/* add post */
CREATE (a:u7pasterczyk:post { name: $post_id, content: $content, author: $author, score: 0 })
RETURN a

/* like post */
MATCH (n:u7pasterczyk:post { name: $post_id })
SET n.score = n.score + 1
RETURN n

/* add comment */
MATCH (a:u7pasterczyk)
WHERE a.name = $obj_id
CREATE (b:u7pasterczyk:comment { name: $comment_id, content: $comment, author: $author})
CREATE (b)-[r:BELONGS]->(a)
RETURN a, b

/* get posts */
MATCH (a:u7pasterczyk:post)
RETURN a

/* get comments */
MATCH (a:u7pasterczyk { name: $parent_id })<-[:BELONGS]-(b)
RETURN b

/* delete post/comment tree */
MATCH (a:u7pasterczyk)-[*..]->(b:u7pasterczyk) WHERE b.name=$parent_id
DETACH DELETE a, b


/* example1 */
CREATE (a:u7pasterczyk:post { name: 'id-12', content: "Hi im new there", author: "Me", score: 21 }) 

/* example2 */
MATCH (a:u7pasterczyk)
WHERE a.name = 'id-21'
CREATE (b:u7pasterczyk:comment { name: 'id-1', content: '1/2', author: 'Hihi'})
CREATE (b)-[r:BELONGS]->(a)
RETURN a, b

/* example3 */
MATCH (a:u7pasterczyk { name: 'id-21' })<-[:BELONGS]-(b)
RETURN b

/* example4 */
MATCH (a:u7pasterczyk)-[*..]->(b:u7pasterczyk) WHERE b.name='52161490-0d78-46c9-95ba-d3122ac54782' RETURN a, b