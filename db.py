from neo4j import GraphDatabase
import uuid

str_add_post = """
    CREATE (a:u7pasterczyk:post { name: $post_id, content: $content, author: $author, score: 0 })
    RETURN a
"""

str_like = """
    MATCH (n:u7pasterczyk:post { name: $post_id })
    SET n.score = n.score + 1
    RETURN n
"""

str_add_comment = """
    MATCH (a:u7pasterczyk)
    WHERE a.name = $obj_id
    CREATE (b:u7pasterczyk:comment { name: $comment_id, content: $content, author: $author })
    CREATE (b)-[r:BELONGS]->(a)
    RETURN b
"""

str_get_posts = """
    MATCH (a:u7pasterczyk:post)
    RETURN a
"""

str_get_comments = """
    MATCH (a:u7pasterczyk { name: $parent_id })<-[:BELONGS]-(b)
    RETURN b
"""

str_delete_children = """
    MATCH (a:u7pasterczyk)-[*..]->(b:u7pasterczyk) WHERE b.name=$parent_id
    DETACH DELETE a
"""

str_delete_parent = """
    MATCH (b:u7pasterczyk) WHERE b.name=$parent_id
    DETACH DELETE b
"""

class Connection:

    def __init__(self, uri="bolt://neo4j.fis.agh.edu.pl:7687", user="u7pasterczyk", password="297901"):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        self.driver.close()

    def execute_add_post(self, content, author):
        with self.driver.session() as session:
            return session.write_transaction(Connection.add_post, content, author)

    def execute_like(self, post_id):
        with self.driver.session() as session:
            return session.write_transaction(Connection.like, post_id)

    def execute_add_comment(self, obj_id, content, author):
        with self.driver.session() as session:
            return session.write_transaction(Connection.add_comment, obj_id, content, author)

    def execute_get_posts(self):
        with self.driver.session() as session:
            return session.write_transaction(Connection.get_posts)

    def execute_get_comments(self, parent_id):
        with self.driver.session() as session:
            return session.write_transaction(Connection.get_comments, parent_id)

    def execute_delete(self, parent_id):
        with self.driver.session() as session:
            return session.write_transaction(Connection.delete_obj, parent_id)


    @staticmethod
    def add_post(session, content, author):
        id = str(uuid.uuid4())
        return session.run(str_add_post, post_id=id, content=content, author=author).data()

    @staticmethod
    def like(session, post_id):
        return session.run(str_like, post_id=post_id).data()[0]["n"]

    @staticmethod
    def add_comment(session, obj_id, content, author):
        id = str(uuid.uuid4())
        return session.run(str_add_comment, obj_id=obj_id, comment_id=id, content=content, author=author).data()

    @staticmethod
    def get_posts(session):
        return session.run(str_get_posts).data()

    @staticmethod
    def get_comments(session, parent_id):
        session.run(str_get_comments, parent_id=parent_id).data()
        return session.run(str_get_comments, parent_id=parent_id).data()

    @staticmethod
    def delete_obj(session, parent_id):
        session.run(str_delete_children, parent_id=parent_id).data()
        return session.run(str_delete_parent, parent_id=parent_id).data()