from flask import Flask, redirect, request
from flask.wrappers import Response
import db
import json

app = Flask(__name__)
instance = db.Connection()


@app.route('/')
def home():
    return redirect("static/app.html", code=302)


@app.route('/api')
def hello():
    return "/api/new/post<br>/api/like<br>/api/new/comment<br>/api/posts<br>/api/comments"


@app.route('/api/new/post', methods=['PUT'])
def new_post():
    content = request.get_json()
    response = instance.execute_add_post(content["content"], content["author"])
    result = {
        "result": "OK",
    }
    return json.dumps(result, indent=4)


@app.route('/api/like/<name>', methods=['GET', 'POST'])
def like(name):
    response = instance.execute_like(name)
    return json.dumps(response, indent=4)


@app.route('/api/new/comment', methods=['PUT'])
def new_comment():
    content = request.get_json()
    print(content)
    response = instance.execute_add_comment(content["parent_id"], content["content"], content["author"])
    result = {
        "result": "OK",
    }
    return json.dumps(result, indent=4)


@app.route('/api/posts', methods=['GET'])
def get_posts():
    data = instance.execute_get_posts()

    response = {
        "data": [x["a"] for x in data]
    }

    return json.dumps(response, indent=4)


@app.route('/api/comments/<name>', methods=['GET'])
def get_comments(name):

    data = instance.execute_get_comments(name)

    response = {
        "data": [x["b"] for x in data]
    }
    
    return json.dumps(response, indent=4)


@app.route('/api/delete/<name>', methods=['DELETE'])
def delete(name):
    data = instance.execute_delete(name)
    
    result = {
        "result": "OK",
    }
    return json.dumps(result, indent=4)


if __name__ == '__main__':
    app.run()