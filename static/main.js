window.onload = function() {
    getPosts();
};

let POSTS = null;

function getPosts() {
    $.ajax({
        url: "/api/posts",
        type: "GET",
        contentType: "application/json",
        success: function(data) {
            // console.log(data);
            POSTS = JSON.parse(data)["data"];
            populatePosts();
        },
        error: function(xhr, status, error) {
            const code = parseInt(xhr.status);
        }
    });

}

function populatePosts() {
    $("#content-container").empty();
    for(var item of POSTS) {
        console.log(item);
        const element = $.parseHTML(Mustache.render(post_str, item))
        $("#content-container").append(element);
    }
}

function rebuild() {
    getPosts();
}

function deleteObject(obj_id) {
    console.log(obj_id);

    $.ajax({
        url: "/api/delete/" + obj_id,
        type: "DELETE",
        contentType: "application/json",
        success: function(data) {
            console.log(data);
            rebuild();
        },
        error: function(xhr, status, error) {
            const code = parseInt(xhr.status);
        }
    });
}

function likePost(obj_id) {
    console.log(obj_id);

    $.ajax({
        url: "/api/like/" + obj_id,
        type: "GET",
        contentType: "application/json",
        success: function(data) {
            console.log(data);
            rebuild();
        },
        error: function(xhr, status, error) {
            const code = parseInt(xhr.status);
        }
    });
}

function newPost() {

    const post_data = {
        content: $("#post-content").val(),
        author: $("#author").val()
    };

    console.log(post_data);

    $.ajax({
        url: "/api/new/post",
        type: "PUT",
        data: JSON.stringify(post_data),
        contentType: "application/json",
        success: function(data) {
            console.log(data);
            rebuild();
        },
        error: function(xhr, status, error) {
            const code = parseInt(xhr.status);
        }
    });
}

function newComment() {

    const obj_id = $("#commented-obj-id").val();

    const comment_data = {
        content: $("#comment-content").val(),
        author: $("#author").val(),
        parent_id: obj_id
    };

    console.log(comment_data);

    $.ajax({
        url: "/api/new/comment",
        type: "PUT",
        data: JSON.stringify(comment_data),
        contentType: "application/json",
        success: function(data) {
            console.log(data);
        },
        error: function(xhr, status, error) {
            const code = parseInt(xhr.status);
        }
    });
}

// get comments

function populateComments(obj_id, data) {
    console.log(data);

    const dom_id = "#" + obj_id;
    $(dom_id).empty();

    for(var item of data) {
        console.log(item);
        const element = $.parseHTML(Mustache.render(comment_str, item))
        $(dom_id).append(element);
    }
}

function getComments(obj_id) {
    console.log(obj_id);

    $.ajax({
        url: "/api/comments/" + obj_id,
        type: "GET",
        contentType: "application/json",
        success: function(data) {
            populateComments(obj_id, JSON.parse(data)["data"]);
        },
        error: function(xhr, status, error) {
            const code = parseInt(xhr.status);
        }
    });
}

function fillCommentId(obj_id) {
    $("#commented-obj-id").val(obj_id);
}