const post_str = `
<div>
    <div class="post">
        <div class="content-box">
            {{ content }}
        </div>

        <div class="bottom-box">
            <div class="score-box">
                <div>
                    Popularność
                </div>
                <div>
                    {{ score }}
                </div>
            </div>
            <div class="action-box">
                <button class="action-button" value="{{ name }}" onclick="likePost(this.value)">+1</button>
                <button class="action-button" value="{{ name }}" onclick="fillCommentId(this.value)">Skomentuj</button>
                <button class="action-button" value="{{ name }}" onclick="deleteObject(this.value)">Usuń</button>
                <button class="action-button" value="{{ name }}" onclick="getComments(this.value)">Pokaż komentarze</button>
            </div>
        </div>
    </div>
    <div class="children" id="{{ name }}">

    </div>
</div>
`;

const comment_str = `
<div>
    <div class="post">
        <div class="content-box">
            {{ content }}
        </div>

        <div class="bottom-box">
            <div class="action-box">
                <button class="action-button" value="{{ name }}" onclick="fillCommentId(this.value)">Skomentuj</button>
                <button class="action-button" value="{{ name }}" onclick="deleteObject(this.value)">Usuń</button>
                <button class="action-button" value="{{ name }}" onclick="getComments(this.value)">Pokaż komentarze</button>
            </div>
        </div>
    </div>
    <div class="children" id="{{ name }}">

    </div>
</div>
`;